# Translations template for rome.
# Copyright (C) 2011 ORGANIZATION
# This file is distributed under the same license as the rome project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: rome 0.1.7\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2011-10-26 13:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 0.9.6\n"

#: tests/field/test_field_list.py:25 rome/__init__.py:122
#, python-format
msgid "%(max)i items maximum permitted"
msgstr ""

#: tests/field/test_field_list.py:29 rome/__init__.py:123
#, python-format
msgid "%(min)i items minimum permitted"
msgstr ""

#: tests/schema/test_schema_nested.py:32 tests/schema/test_schema_nested.py:36
#: tests/schema/test_schema_nested.py:40 tests/schema/test_schema_simple.py:51
#: tests/schema/test_schema_simple.py:55 tests/schema/test_schema_simple.py:56
#: tests/schema/test_schema_simple.py:60 rome/__init__.py:181
msgid "Missing value"
msgstr ""

#: tests/schema/test_schema_simple.py:64 rome/__init__.py:182
msgid "Forbidden by conditions"
msgstr ""

#: tests/validators/test_email.py:17 tests/validators/test_email.py:21
#: tests/validators/test_email.py:25 tests/validators/test_email.py:29
#: rome/validators.py:90
msgid "This is not a valid email"
msgstr ""

#: tests/validators/test_in.py:18 rome/validators.py:76
#, python-format
msgid "Value must be in list [%(values)s]"
msgstr ""

#: tests/validators/test_int.py:17 tests/validators/test_int.py:21
#: rome/validators.py:66
msgid "This is not an integer"
msgstr ""

#: tests/validators/test_number.py:29 rome/validators.py:49
msgid "This is not a number"
msgstr ""

#: tests/validators/test_string.py:24 tests/validators/test_string.py:35
#: rome/validators.py:9
msgid "This is not a String"
msgstr ""

#: tests/validators/test_string.py:30 rome/validators.py:10
msgid "Please enter a value"
msgstr ""

#: tests/validators/test_string.py:41
msgid "Max value must be greater than min"
msgstr ""

#: tests/validators/test_string.py:46 rome/validators.py:11
#, python-format
msgid "Value length must be %(length)i exactly"
msgstr ""

#: tests/validators/test_string.py:51 rome/validators.py:12
#, python-format
msgid "Value length must be %(max)i or less"
msgstr ""

#: tests/validators/test_string.py:56 rome/validators.py:13
#, python-format
msgid "Value length must be %(min)i or more"
msgstr ""

#: rome/__init__.py:183
msgid "Invalid data type"
msgstr ""

