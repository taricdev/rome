��          �                         =     ]     u  "   �     �     �     �     �     �       '   (  $   P  $   u  "   �  �  �  *   h  *   �  &   �     �  3   �     0     B  !   V  !   x  -   �      �  4   �  3     3   R  /   �   %(max)i items maximum permitted %(min)i items minimum permitted Forbidden by conditions Invalid data type Max value must be greater than min Missing value Please enter a value This is not a String This is not a number This is not a valid email This is not an integer Value length must be %(length)i exactly Value length must be %(max)i or less Value length must be %(min)i or more Value must be in list [%(values)s] Project-Id-Version: rome 0.1
Report-Msgid-Bugs-To: cesarpereziglesias@gmail.com
POT-Creation-Date: 2011-10-26 13:26+0200
PO-Revision-Date: 2011-04-19 16:44+0200
Last-Translator: Cesar Alberto Perez Iglesias <cesarpereziglesias@gmail.com>
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.6
 Se permiten %(max)i elementos como máximo Se permiten %(min)i elementos como mínimo Prohibido por determinadas condiciones Tipo de dato inválido El valor máximo tiene que ser mayor que el mínimo Campo obligatorio Introduzca un valor El valor tiene que ser una cadena El valor tiene que ser un número El formato del email introducido no es valido El valor tiene que ser un entero El tamaño de la cadena tienes que ser de %(length)i El tamaño de la cadena tiene un máximo de %(max)i El tamaño de la cadena tiene un mínimo de %(min)i El valor debe de estar en la lista [%(values)s] 