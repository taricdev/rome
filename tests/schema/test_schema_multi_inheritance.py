# -*- coding: utf-8 -*-

from nose.tools import assert_true

from rome import Schema, Field, FieldCombined, FieldConstant

from tests import SUTValidator, SUTCombinedValidator


class SUTSchemaA(Schema):
    field = Field(SUTValidator())
    field_a = Field(SUTValidator())
    constant_a = FieldConstant('foo')
    combined_a = FieldCombined(SUTCombinedValidator('field', 'field_a'), destination='field_a')

class SUTSchemaB(Schema):
    field = Field(SUTValidator())
    field_b = Field(SUTValidator())
    constant_b = FieldConstant('foo')
    combined_b = FieldCombined(SUTCombinedValidator('field', 'field_b'), destination='field_b')

class SUTSchemaAB(SUTSchemaA, SUTSchemaB):
    pass

def test_multi_inheritance():
    schema = SUTSchemaAB()
    for field in ('field', 'field_a', 'field_b', 'constant_a', 'constant_b', 'combined_a', 'combined_b'):
        assert_true(hasattr(schema, field))
