#-*- coding: utf-8 -*-

from nose.tools import raises

from rome import Schema, FieldCalculated

from tests import _TestValidator

class SUTSchema(Schema):

    field = FieldCalculated(lambda self, values: 'foo')

class TestFieldCalculated(_TestValidator):

    VALIDATOR = SUTSchema()

    def test_calculated(self):
        self.data = {}
        self.data_ok(expected={'field': 'foo'})

@raises(TypeError)
def test_not_callable():
    class TypeErrorSchema(Schema):
        field = FieldCalculated('foo')

    TypeErrorSchema().validate({})
