#-*- coding: utf-8 -*-

from nose.tools import assert_equals

from rome import Schema, Field
from rome.validators import String

from tests import SUTValidator, _TestValidator

class SUTSchema(Schema):

    field = Field(SUTValidator(), default='foo')

class TestFieldDefaults(_TestValidator):

    VALIDATOR = SUTSchema()

    def test_given_value(self):
        self.data = {'field': 'bar'}
        self.data_ok()

    def test_no_given_value(self):
        self.data = {}
        self.data_ok(expected={'field': 'foo'})

def test_callable_value():
    class SUTSchema(Schema):
        field = Field(SUTValidator(), default=lambda self, values: 'foo')

    assert_equals(SUTSchema().validate({}), {'field': 'foo'})

class SUTSchemaConstant(Schema):

    field = Field(String(), default='foo')

class TestFieldConstant(_TestValidator):

    VALIDATOR = SUTSchemaConstant()

    def test_constant(self):
        self.data = {}
        self.data_ok(expected={'field': 'foo'})
