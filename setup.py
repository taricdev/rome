# -*- coding: utf-8 -*-
"""
This file is part of rome.

rome is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with rome.  If not, see <http://www.gnu.org/licenses/>.
"""

from setuptools import setup, find_packages

NAME = 'rome'
VERSION = '0.2.0'

requires = ['Babel',
            'nose',
            'coverage']

setup(name=NAME,
      version=VERSION,
      packages=find_packages(),
      install_requires=requires,
      package_data={'rome': ['locale/*/LC_MESSAGES/*.mo']},
      tests_require=requires,
      test_suite="nose.collector")
